import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-importcontacts',
  templateUrl: 'importcontacts.html',
})
export class ImportcontactsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
  }

    addbuddy() {
        this.navCtrl.push('ContactPage', {source:"ImportcontactsPage"});
    }

}
