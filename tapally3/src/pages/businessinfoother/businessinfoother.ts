import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,App } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-businessinfoother',
  templateUrl: 'businessinfoother.html',
})
export class BusinessinfootherPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public app: App) {
  }

  backButtonClick (){
    this.navCtrl.pop({animate: true, direction: "forward"});
  }

  addbuddy() {
      this.app.getRootNav().push('ContactPage', {source:"ImportpPage"});
  }

  sendToBusinessReg (){
    this.app.getRootNav().push("RegisterbusinessPage", {treatNewInstall : 0});
  }

}
