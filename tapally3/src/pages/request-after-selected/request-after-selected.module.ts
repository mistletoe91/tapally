import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RequestAfterSelectedPage } from './request-after-selected';

@NgModule({
  declarations: [
    RequestAfterSelectedPage,
  ],
  imports: [
    IonicPageModule.forChild(RequestAfterSelectedPage),
  ],
})
export class RequestAfterSelectedPageModule {}
