import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchChatsPage } from './search-chats';

@NgModule({
  declarations: [
    SearchChatsPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchChatsPage),
  ],
})
export class SearchChatsPageModule {}
