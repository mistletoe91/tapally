import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContactimportprogressPage } from './contactimportprogress';

@NgModule({
  declarations: [
    ContactimportprogressPage,
  ],
  imports: [
    IonicPageModule.forChild(ContactimportprogressPage),
  ],
})
export class ContactimportprogressPageModule {}
