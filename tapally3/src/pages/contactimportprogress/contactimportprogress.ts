import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Events } from 'ionic-angular';
import { ContactProvider } from '../../providers/contact/contact';
import { TpstorageProvider } from '../../providers/tpstorage/tpstorage';

@IonicPage()
@Component({
  selector: 'page-contactimportprogress',
  templateUrl: 'contactimportprogress.html',
})
export class ContactimportprogressPage {
  userId: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public contactProvider: ContactProvider,
    public tpStorageService: TpstorageProvider,
    public events: Events
  ) {
    this.tpStorageService.getItem('userUID').then((res: any) => {
			 if(res){
				 this.userId = res;
			 }
		}).catch(e => { });


  }


    ionViewDidLoad() {
       if(this.navParams.get('source') == "ContactPage"){
              //first run
              this.refersh ('ContactPage');
       } else {

              if(this.navParams.get('source') == "PickContactFromPage"){
                  this.refersh ('TabsPage');//mytodo : this should go to PickContactFromPage. but make sure u do .pop instead of setroot . but u have to do carefully. test it throughoutly
              } else {
                if(this.navParams.get('source') == "PickContactToPage"){
                  this.refersh ('TabsPage');//mytodo : this should go to PickContactFromPage. but make sure u do .pop instead of setroot . but u have to do carefully. test it throughoutly 
                } else {
                  this.ionViewDidLoad_ ();
                }
              }

       }
    }

    refersh (nextPage){
      //first run
      this.contactProvider.getSimJsonContacts()
      .then(res => {
          this.tpStorageService.setItem('contactsImportedFromDevice', '1');
          this.events.subscribe('contactInsertingDone', () => {
            this.events.unsubscribe('contactInsertingDone');
            if(!this.contactProvider.contactInsertingDone){
            } else {
              this.navCtrl.push(nextPage, {source:"ImportpPage"});
            }
          });
      }).catch(e => { });
    }

  	ionViewDidLoad_() {

      this.tpStorageService.getItem('contactsImportedFromDevice').then((res: any) => {
        //contacts already imported
          if(res || res =="1"){
             this.sendToContactsPage();
          }
      }).catch(e => {
          this.refersh ("ContactPage");
      });

  	}//end function


  sendToContactsPage() {
    //this.navCtrl.push('InvitemanuallyPage');
    this.navCtrl.push("ContactPage", {source:"ImportpPage"});
  }

}
