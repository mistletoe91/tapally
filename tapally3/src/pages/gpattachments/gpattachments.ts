import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, PopoverController } from 'ionic-angular';

import { GroupsProvider } from '../../providers/groups/groups';
import { UserProvider } from '../../providers/user/user';
import { LoadingProvider } from '../../providers/loading/loading';
import { ImagehandlerProvider } from '../../providers/imagehandler/imagehandler';
import { ChatProvider } from '../../providers/chat/chat';
import { FcmProvider } from '../../providers/fcm/fcm';
import { App } from 'ionic-angular';
@IonicPage()
@Component({
    selector: 'page-gpattachments',
    templateUrl: 'gpattachments.html',
})
export class GpattachmentsPage {

    loginuserInfo: any = [];
    gpname:any;
    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public userservice: UserProvider,
        public loading: LoadingProvider,
        public imgstore: ImagehandlerProvider,
        public chatservice: ChatProvider,
        public fcm: FcmProvider,
        private viewCtrl: ViewController,
        public groupservice: GroupsProvider,
        public popoverCtrl: PopoverController,
        public app:App
    ) {
    }
    ionViewDidLoad() {
        this.gpname = this.navParams.get('gname');

        this.userservice.getuserdetails().then((res: any) => {
            this.loginuserInfo = res;
        }).catch(err => {
        });
    }
    sendGalleryPicMsg() {
        this.dismissMe();
        this.imgstore.picmsgstore().then((imgurl: any) => {
            if (imgurl.length > 4) {
                let flag = 0;
                this.groupservice.addgroupmsgmultiple(imgurl, this.loginuserInfo, 'image',this.gpname).then((res) => {
                    if (res) {
                        flag++;
                    }
                    if (flag == imgurl.length) {
                    }
                })
            } else {
                let flag = 0
                for (let i = 0; i < imgurl.length; i++) {
                    this.groupservice.addgroupmsg(imgurl[i], this.loginuserInfo, 'image',this.gpname).then((res) => {
                        if (res) {
                            flag++;
                        }
                        if (flag == imgurl.length) {

                        }
                    })
                }
            }
        }).catch((err) => {
        })
    }
    dismissMe (){
      if(this.viewCtrl){
        this.viewCtrl.dismiss();
      }
    }
    sendPicMsg() {
        this.dismissMe();
        let flag = 0
        this.imgstore.cameraPicmsgStore().then((imgurl) => {
            this.groupservice.addgroupmsg(imgurl, this.loginuserInfo, 'image',this.gpname).then((res) => {
                if (res) {
                    flag++;
                }
            })
        }).catch((err) => {
        })
    }
    sendDocument() {
          this.dismissMe();
        let flag = 0
        this.imgstore.selectDocument().then((imgurl) => {
            this.groupservice.addgroupmsg(imgurl, this.loginuserInfo, 'document', this.gpname).then((res) => {
                if (res) {
                    flag++;
                }

            })
        }).catch((err) => {
        })
    }

    sendAudioMsg() {
        this.dismissMe();
        let flag = 0
        this.imgstore.recordAudio().then((imgurl) => {
            this.groupservice.addgroupmsg(imgurl, this.loginuserInfo, 'audio' ,this.gpname).then((res) => {
                if (res) {
                    flag++;
                }
            })

        }).catch((err) => {
            this.loading.dismissMyLoading();
        })
    }

    openContacts(myEvent) {
        this.viewCtrl.dismiss({page:"AllcontactsPage",gpname:this.gpname});
    }

    openMap(myEvent) {
        this.viewCtrl.dismiss({page:"MapPage",gpname:this.gpname,flag: true});
    }
}
/*
Original Text in html
<ion-item no-line>
    <ion-label (click)="sendDocument()" no-line >Documents</ion-label>
  </ion-item>
  <ion-item (click)="sendPicMsg()">
    <ion-label no-line >Camera</ion-label>
  </ion-item>
  <ion-item (click)="sendGalleryPicMsg()" no-line>
    <ion-label no-line >Gallery</ion-label>
  </ion-item>
  <!-- <ion-item no-line (click)="sendAudioMsg()">
    <ion-label no-line >Audio</ion-label>
  </ion-item> -->
  <ion-item no-line (click)="openContacts($event)">
    <ion-label no-line >Contact</ion-label>
  </ion-item>
  <ion-item no-line (click)="openMap($event)">
    <ion-label no-line >Current Location</ion-label>
  </ion-item>
*/
