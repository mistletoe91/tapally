import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { ContactProvider } from '../../providers/contact/contact';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';


@IonicPage()
@Component({
  selector: 'page-add-contact',
  templateUrl: 'add-contact.html',
})
export class AddContactPage {
  contactObject = {
    displayName: '',
    nickName: '',
    phoneNumber: '',
    phoneType: 'mobile'
  }
  authForm: FormGroup;
  displayNam: AbstractControl;
  phoneNumbe: AbstractControl;
  nickName: AbstractControl;
  submitAttempt: boolean = false;
  showheader:boolean = true;
  contacts: any = [];
  msgstatus: boolean = false;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public contactProvider: ContactProvider,
    public viewCtrl: ViewController,
    private alertCtrl: AlertController,
    public fb: FormBuilder
  ) {
    this.authForm = this.fb.group({
      'displayNam': [null, Validators.compose([Validators.required])],
      'phoneNumbe': [null, Validators.compose([Validators.required])],
      'nickName': [null],
    });
    this.displayNam = this.authForm.controls['displayNam'];
    this.phoneNumbe = this.authForm.controls['phoneNumbe'];
    this.nickName = this.authForm.controls['nickName'];
  }
  ionViewDidLoad() {

    if (this.navParams.get('contacts')) {
      this.contacts = this.navParams.get('contacts');
      this.contactObject.displayName = this.contacts.displayName.trim();
      this.contactObject.phoneNumber = this.contacts.mobile;

    }
  }
  ionViewWillLeave() {
     this.showheader = false;
  }
  onChange(e) {
    if (this.contactObject.displayName.trim()) {
      this.msgstatus = true;
    }
    else {
      this.msgstatus = false;
    }
  }
  saveContact(newContact: any) {
    this.submitAttempt = true;
    if (this.authForm.valid) {
      this.contactProvider.addContact(newContact).then((res) => {
        if (res) {
          this.viewCtrl.dismiss();
        }
      });
    }
  }
  closeContact() {
    let alert = this.alertCtrl.create({
      title: 'Discard changes',
      message: 'Discard your changes?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'Ok',
          handler: () => {
            this.viewCtrl.dismiss();
          }
        }
      ]
    });
    alert.present();
  }
}
