import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReferralreceivedPage } from './referralreceived';

@NgModule({
  declarations: [
    ReferralreceivedPage,
  ],
  imports: [
    IonicPageModule.forChild(ReferralreceivedPage),
  ],
})
export class ReferralreceivedPageModule {}
