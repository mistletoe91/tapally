import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,App } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-businessinfo',
  templateUrl: 'businessinfo.html',
})

//mytodo : Right now we are hardcoring the copy. Please use webview to pull content directly from website. Use a dedicated page in website to render it here
export class BusinessinfoPage {

  showPaidSign:boolean = false;
  myLink;
  constructor(public app: App, public navCtrl: NavController, public navParams: NavParams) {
      if(navParams.get('showPaidSign')){
        this.showPaidSign = true;
        this.myLink = navParams.get('myLink');
      }
  }
 

  backButtonClick (){
    this.navCtrl.pop({animate: true, direction: "forward"});
  }

  addbuddy() {
      this.app.getRootNav().push('ContactPage', {source:"ImportpPage"});
  }

  sendToBusinessReg (){
    this.app.getRootNav().push("RegisterbusinessPage", {treatNewInstall : 0});
  }

}
