import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShowContactPage } from './show-contact';

@NgModule({
  declarations: [
    ShowContactPage,
  ],
  imports: [
    IonicPageModule.forChild(ShowContactPage),
  ],
})
export class ShowContactPageModule {}
