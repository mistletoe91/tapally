// Angular core
import { Component, NgZone } from '@angular/core';

// Ionic-angular
import { IonicPage, NavController, NavParams, LoadingController, ModalController, AlertController, Events, ViewController, Platform } from 'ionic-angular';

// Providers
import { ImagehandlerProvider } from '../../providers/imagehandler/imagehandler';
import { UserProvider } from '../../providers/user/user';
import { LoadingProvider } from '../../providers/loading/loading';
import { RequestsProvider } from '../../providers/requests/requests';
import { GroupsProvider } from '../../providers/groups/groups';
// Firebase
import firebase from 'firebase';

// Ionic image viewer
import { ImageViewerController } from 'ionic-img-viewer';
import { env } from '../../app/app.angularfireconfig';
import { TpstorageProvider } from '../../providers/tpstorage/tpstorage';
@IonicPage()
@Component({
    selector: 'page-profilepic',
    templateUrl: 'profilepic.html',
})
export class ProfilepicPage {

    imgurl = env.userPic;
    userId;
    isDisabled: boolean = false;
    profileData: any = {
        username: '',
        disc: '',
        imgurl: env.userPic
    }
    usernamePlaceholder = "Enter your name";
    discPlaceholder = 'About yourself';
    name;
    img;
    _imageViewerCtrl: ImageViewerController;
    showheader:boolean = true;

    constructor(public navCtrl: NavController, public navParams: NavParams,
        public imgservice: ImagehandlerProvider,
        public zone: NgZone,
        public events: Events,
        public loadingProvider: LoadingProvider,
        public userservice: UserProvider,
        public loadingCtrl: LoadingController,
        public modalCtrl: ModalController,
        public alertCtrl: AlertController,
        public imageViewerCtrl: ImageViewerController, public viewCtrl: ViewController,
        public platform: Platform,
        public requestservice: RequestsProvider,
        public groupservice: GroupsProvider,
        public tpStorageService: TpstorageProvider

    ) {
        this.name = navParams.get('name');
        this.img = navParams.get('img');
        this._imageViewerCtrl = imageViewerCtrl;
    }
    ionViewWillLeave() {
       this.showheader = false;
    }
    presentImage(myImage) {
        const imageViewer = this._imageViewerCtrl.create(myImage);
        imageViewer.present();
    }
    ionViewWillEnter() {
        this.loadingProvider.presentLoading();
        this.tpInitilizeFromStorage ();

    }
    tpInitilizeFromStorage (){
      this.tpStorageService.getItem('userUID').then((res: any) => {
         if(res){
           this.userId = res;
           this.loaduserdetails();
         }
      }).catch(e => { });
    }
    loaduserdetails() {
        let userId = this.userId;
        if (userId != undefined) {
            this.isDisabled = true;
            this.userservice.getuserdetails().then((res: any) => {
                this.loadingProvider.dismissMyLoading();
                if (res) {
                    let desc = res['disc'].replace(new RegExp('\n', 'g'), "<br />");
                    if (res['displayName'] != undefined) {
                        this.profileData.username = res['displayName'];
                    }
                    if (res['photoURL'] != undefined) {
                        this.profileData.imgurl = res['photoURL'];
                    }
                    if (desc != undefined) {
                        this.profileData.disc = desc;
                    }
                }
            }).catch(err => {
            });
        } else {
            this.loadingProvider.dismissMyLoading();
            this.profileData = {
                username: '',
                disc: '',
                imgurl: env.userPic
            }
        }
    }
    backButtonClick() {
        this.navCtrl.pop({animate: true, direction: "forward"});
    }


    choseImage() {
        console.log ("Choose Image");
        this.imgservice.uploadimage().then((uploadedurl: any) => {
            this.loadingProvider.dismissMyLoading();
            this.zone.run(() => {
                 this.profileData.imgurl = uploadedurl;
                this.updateproceed(this.profileData.imgurl);
            })
        }).catch(err => {
            if (err == 20) {
                this.navCtrl.pop();
            }
            this.loadingProvider.dismissMyLoading();
        });
    }

    back() {
        this.navCtrl.pop();
    }
    updateproceed(imgurl) {
        this.userservice.updateimage(imgurl).then((res: any) => {
            if (res.success) {
                console.log ("Userservice is updated with imgurl");
            }
            else {
                //alert(res);
            }
        })
    }
    proceed() {
        this.userservice.directLogin().then((data: any) => {
            if (data.success) {
                this.navCtrl.setRoot('TabsPage');
            }
        })
    }
    username(username) {
        let profileModal = this.modalCtrl.create("UsernamePage", { userName: username });
        profileModal.onDidDismiss(data => {

            if (data != undefined) {
                this.profileData.username = data.username;
                this.userservice.updatedisplayname(data.username).then((res: any) => {
                    if (res.success) {
                    }
                })
            } else {
            }
        });
        profileModal.present();
    }
    discription(disc) {
        let profileModal = this.modalCtrl.create("DiscPage", { disc: disc });
        profileModal.onDidDismiss(data => {
            if (data != undefined) {
                data.disc = data.disc.replace(new RegExp('\n', 'g'), "<br />");
                this.profileData.disc = data.disc;
                this.userservice.updatedisc(data.disc).then((res: any) => {
                    if (res.success) {
                    }
                })
            } else {
            }
        });
        profileModal.present();
    }
    deleteProfile() {
        let alert = this.alertCtrl.create({
            title: 'Delete User',
            message: 'Do you want to delete this Account?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                    }
                },
                {
                    text: 'Ok',
                    handler: () => {

                        this.userservice.deleteUser().then((data: any) => {
                            if (data.success) {
                                this.navCtrl.setRoot('PhonePage');
                            } else {
                            }
                        }).catch((err) => {
                        })
                    }
                }
            ]
        });
        alert.present();
    }
}
