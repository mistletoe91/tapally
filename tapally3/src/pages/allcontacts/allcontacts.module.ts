import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AllcontactsPage } from './allcontacts';

@NgModule({
  declarations: [
    AllcontactsPage,
  ],
  imports: [
    IonicPageModule.forChild(AllcontactsPage),
  ],
})
export class AllcontactsPageModule {}
