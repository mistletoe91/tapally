import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DisplaynamePage } from './displayname';

@NgModule({
  declarations: [
    DisplaynamePage,
  ],
  imports: [
    IonicPageModule.forChild(DisplaynamePage),
  ],
})
export class DisplaynamePageModule {}
