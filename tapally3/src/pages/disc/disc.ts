import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-disc',
  templateUrl: 'disc.html',
})
export class DiscPage {
  disc;
  limitField;
  countmodel:any;
  max:number=5;
  longTxt :number = 250;
  showheader:boolean = true;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl : ViewController) {
    this.disc = this.navParams.get('disc');
    this.disc = this.disc.replace(new RegExp('<br />', 'g'), "\n");
  }

   limitText(event:Event) {
     var limitCount = (+(this.disc.length)) ;

     if(limitCount > this.longTxt){
       this.disc = this.disc.slice(0, this.longTxt);
     }else{
       limitCount = this.longTxt - limitCount;
     }


    }


  ionViewWillLeave() {
       this.showheader = false;
  }
  ionViewDidLoad() {
  }
  save() {
   let data = { disc:this.disc };

   this.viewCtrl.dismiss(data);
 }
 dismiss(){
   this.viewCtrl.dismiss();
 }


}
