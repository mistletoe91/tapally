import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchNextPage } from './search-next';

@NgModule({
  declarations: [
    SearchNextPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchNextPage),
  ],
})
export class SearchNextPageModule {}
