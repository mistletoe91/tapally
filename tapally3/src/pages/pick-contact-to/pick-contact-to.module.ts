import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PickContactToPage } from './pick-contact-to';

@NgModule({
  declarations: [
    PickContactToPage,
  ],
  imports: [
    IonicPageModule.forChild(PickContactToPage),
  ],
})
export class PickContactToPageModule {}
