import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RequestSubmittedPage } from './request-submitted';

@NgModule({
  declarations: [
    RequestSubmittedPage,
  ],
  imports: [
    IonicPageModule.forChild(RequestSubmittedPage),
  ],
})
export class RequestSubmittedPageModule {}
