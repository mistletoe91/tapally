import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IncentivedetailPage } from './incentivedetail';

@NgModule({
  declarations: [
    IncentivedetailPage,
  ],
  imports: [
    IonicPageModule.forChild(IncentivedetailPage),
  ],
})
export class IncentivedetailPageModule {}
