import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SendReferralBackwardPage } from './send-referral-backward';

@NgModule({
  declarations: [
    SendReferralBackwardPage,
  ],
  imports: [
    IonicPageModule.forChild(SendReferralBackwardPage),
  ],
})
export class SendReferralBackwardPageModule {}
