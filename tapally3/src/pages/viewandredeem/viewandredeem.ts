import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,App,ToastController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { SQLite,SQLiteObject } from '@ionic-native/sqlite';
import { CONST} from '../../app/const';
import { ChatProvider } from '../../providers/chat/chat';
import { ReferralincentiveTypeProvider } from '../../providers/referralincentive-type/referralincentive-type';
import { TpstorageProvider } from '../../providers/tpstorage/tpstorage';
@IonicPage()
@Component({
  selector: 'page-viewandredeem',
  templateUrl: 'viewandredeem.html',
})
export class ViewandredeemPage {
  referral_received_by;
  referral_send_by;
  requestId;
  pageDoneLoading;
  redeemStatus;
  buddy;
  referralDetail = [];
  oneWhoSendReferral;
  rtlistComplete;
  businessRegistered:boolean = false;
  constructor(private toastCtrl: ToastController,public chatservice: ChatProvider,private sqlite: SQLite,public navCtrl: NavController, public navParams: NavParams,public app: App,public userservice: UserProvider,public rtService : ReferralincentiveTypeProvider,public tpStorageService: TpstorageProvider) {
  }

  ionViewDidLoad (){
      this.tpStorageService.getItem('business_created').then((business_created_raw: any) => {
            //Business Registered
            this.businessRegistered = true;
            this.ionViewDidLoad_async ();
      }).catch(e => {
           //Business not registered
           this.pageDoneLoading = true;
      });
  }

  ionViewDidLoad_async() {
      this.referralDetail.push ({'referral':{'displayName':''}});
      this.rtlistComplete = this.rtService.getList();
     if(this.navParams.get('requestId')){
       this.requestId =  this.navParams.get('requestId');
       this.referral_send_by =  this.navParams.get('referral_send_by');
       this.referral_received_by = this.navParams.get('referral_received_by');

           this.userservice.getbuddydetails(this.referral_send_by).then((ref)=>{
                this.oneWhoSendReferral = ref;
                this.pageDoneLoading = true;
           });

           this.userservice.getReferralRecievedByID(this.referral_send_by,this.referral_received_by,this.requestId).then((referralRecieved)=>{
                let referalType = '';
                //console.log ("referralRecieved");
                //console.log (referralRecieved);
                for(var myReferType in this.rtlistComplete){
                    if(referralRecieved["business_referral_type_id"] == this.rtlistComplete [myReferType].id ) {
                       referralRecieved["business_referral_type_name"] = this.rtlistComplete [myReferType].name;
                       break;
                    }
                }//end for
                this.redeemStatus = referralRecieved ["redeemStatus"];
                this.referralDetail[0] = referralRecieved;
                this.pageDoneLoading = true;
           });


     }
  }

  fnMarkIncentiveRedeemed (){
    this.userservice.fnMarkIncentiveRedeemed_(this.referral_send_by,this.referral_received_by,this.requestId);
    this.chatservice.addnewmessageRedeemDone('Incentive for referral are marked redeemed', 'message', this.referral_send_by, this.requestId,this.referral_send_by,this.referral_received_by).then(() => {
          //mytodo : send user to buddypage
          this.navCtrl.setRoot('TabsPage');
          const toast = this.toastCtrl.create({
            message: 'You have successfully redeemed request for incentives. You can see the status by clicking on your contact`s icon',
            duration: 5000
          });
          toast.present();
    });
  }

  fnSendToBusinessPage (){
    this.navCtrl.push("RegisterbusinessPage", {treatNewInstall : 0});
  }

  backButtonClick() {
    //mytodo : bug : when user go back to buddypage chat does not work properly. Cannot recieve or send messages. Fix that
    //this.navCtrl.pop({animate: true, direction: "right"});

    //temporarliy send to tabs page
    this.navCtrl.setRoot('TabsPage');
  }

}
