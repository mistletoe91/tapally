import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewandredeemPage } from './viewandredeem';

@NgModule({
  declarations: [
    ViewandredeemPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewandredeemPage),
  ],
})
export class ViewandredeemPageModule {}
