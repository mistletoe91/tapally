import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OutreachForBusinessPage } from './outreach-for-business';

@NgModule({
  declarations: [
    OutreachForBusinessPage,
  ],
  imports: [
    IonicPageModule.forChild(OutreachForBusinessPage),
  ],
})
export class OutreachForBusinessPageModule {}
