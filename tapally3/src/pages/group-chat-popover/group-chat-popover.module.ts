import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GroupChatPopoverPage } from './group-chat-popover';

@NgModule({
  declarations: [
    GroupChatPopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(GroupChatPopoverPage),
  ],
})
export class GroupChatPopoverPageModule {}
