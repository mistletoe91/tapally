import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,ViewController, ModalController} from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';

@IonicPage()
@Component({
  selector: 'page-menu-block',
  templateUrl: 'menu-block.html',
})
export class MenuBlockPage {
  buddy:any;
  buddyStatus:boolean = false;
  constructor(
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public userservice : UserProvider,
    public viewCtrl: ViewController
  ) {
    this.buddy =this.navParams.get('buddy')
    if(this.buddy){
      this.getbuddysatus(this.buddy);
    }
  }
  userProfileBlock(){
      this.userservice.blockUser(this.buddy).then((res)=>{
        if(res){
          this.viewCtrl.dismiss();

        }
      })
  }
  userProfileunBlock(){
    this.userservice.unblockUser(this.buddy).then((res)=>{
      if(res){
        this.viewCtrl.dismiss();
      }
    })
  }
  getbuddysatus(buddy){
    this.userservice.getstatus(buddy).then((res:any)=>{
      this.buddyStatus = res;
    })
  }

  searchMessage(myEvent){
    let searchChatModal = this.modalCtrl.create('SearchChatsPage');
		searchChatModal.present({
			ev: myEvent
    });
    searchChatModal.onDidDismiss(()=>{
      this.viewCtrl.dismiss();
    })
		
  }  

}