import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AllimgPage } from './allimg';

@NgModule({
  declarations: [
    AllimgPage,
  ],
  imports: [
    IonicPageModule.forChild(AllimgPage),
  ],
})
export class AllimgPageModule {}
