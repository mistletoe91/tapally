import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController} from 'ionic-angular';
import { ImageViewerController } from 'ionic-img-viewer';
@IonicPage()
@Component({
  selector: 'page-view-buddy',
  templateUrl: 'view-buddy.html',
})
export class ViewBuddyPage {
	name;
	img;
	mobile;
	disc;
	userstatus;
  showheader:boolean = true;
	_imageViewerCtrl: ImageViewerController;
  constructor(public navCtrl: NavController, public navParams: NavParams,  public imageViewerCtrl: ImageViewerController, public viewCtrl: ViewController) {

  	 this.name=navParams.get('name');
  	 this.img=navParams.get('img');
  	 this.mobile=navParams.get('mobile');
		 this.disc=navParams.get('disc');
		 this.userstatus=navParams.get('userstatus');
  	 this.disc = this.disc.replace(/<\/?[^>]+>/gi, "");
  	 this._imageViewerCtrl = imageViewerCtrl;

  }


  ionViewWillLeave() {
     this.showheader = false;
  }
	presentImage(myImage) {
	    const imageViewer = this._imageViewerCtrl.create(myImage);
	    imageViewer.present();
	  }
		goBack() {
			this.navCtrl.pop();
	}


  ionViewDidLoad() {
  }

}
