import { Injectable } from '@angular/core';
import { LoadingController, ToastController } from 'ionic-angular';


@Injectable()
export class LoadingProvider {

	loading:any;

	constructor(public toastCtrl: ToastController,
				public loadingCtrl: LoadingController) {


	}

	// For present loading
	presentLoading() {

			//https://medium.muz.li/top-30-most-captivating-preloaders-for-your-website-95ed1beff99d
			this.loading = this.loadingCtrl.create({
        content: 'Please wait, we are working hard on it'
			});

			this.loading.present().then(()=>{
			   //setTimeout(()=>{
			     // this.loading.dismiss();
			   //},300);
			});

	}
	//dismiss
	dismissMyLoading (){
	  //console.log ("Trying dismiising loading"); 
		if(this.loading){
			this.loading.dismiss();
			this.loading = null;
	  }
	}

	// Toast message
	presentToast(text) {
		let toast = this.toastCtrl.create({
			message: text,
			duration: 3000,
			position: 'top'
		});
		toast.present();
	}

}
