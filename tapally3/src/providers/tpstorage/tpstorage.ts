import { Injectable } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage';
import {  Platform } from 'ionic-angular';
@Injectable()
export class TpstorageProvider {

  /*
  storage
  localStorage,
  nativeStorage
  */
  constructor(public nativeStorage: NativeStorage,public platform: Platform) { 
  }

  clear(){
    this.nativeStorage.clear();
  }

  isArray(obj : any ) {
     return Array.isArray(obj)
  }

  removeItem (nam){
     this.setItem (nam,"");
  }

  set (nam,val){
    if(this.isArray(val)){
      val  = JSON.stringify (val);
    }
    return this.setItem (nam,val);
  }

  get (nam){
    return this.getItem (nam);
  }

  setItem (nam, val){
    if(this.isArray(val)){
      val  = JSON.stringify (val);
    }
    this.nativeStorage.setItem(nam, val).then(
      () => {},
      error => {}
      );
  }

  getItem (nam){
    return this.nativeStorage.getItem(nam);
  }

/*
    return this.nativeStorage.getItem(nam)
    .then(
        data => {},
        error => {console.log ("Tapally_Error_289900 "); console.error(error); return "";}
    );

  }
*/

 /*
getItem(nam) {
  return new Promise((resolve, reject) => {
    this.platform.ready().then((readySource) => {
      //if(this.nativeStorage){
          resolve(this.nativeStorage.getItem(nam));
      //}
    }).catch(e => {
      console.log ("Tapally_Error_283193182 ");
      console.log (e);
      reject(e);
    });
  }).catch(e => {
    console.log ("Tapally_Error_318287 ");
    console.log (e);

    /* Native storage error codes
    NATIVE_WRITE_FAILED = 1
    ITEM_NOT_FOUND = 2
    NULL_REFERENCE = 3
    UNDEFINED_TYPE = 4
    JSON_ERROR = 5
    WRONG_PARAMETER = 6
    */
    /*
  });
}

*/

}
