ionic cordova plugin add cordova-sqlite-storage
npm install --save @ionic-native/sqlite@4


npm install angularfire2 --save
npm install firebase @angular/fire --save

npm install @angular/animations@latest --save

npm i ionic-img-viewer --save

ionic cordova plugin add cordova-plugin-camera
npm install --save @ionic-native/camera@4

ionic cordova plugin add cordova-plugin-contacts
npm install --save @ionic-native/contacts@4

 ionic cordova plugin add cordova-sms-plugin
 npm install --save @ionic-native/sms@4

 ionic cordova plugin add cordova-plugin-inappbrowser
 npm install --save @ionic-native/in-app-browser@4
ionic cordova plugin add cordova-plugin-media-capture
npm install --save @ionic-native/media-capture@4

ionic cordova plugin add cordova-plugin-geolocation --variable GEOLOCATION_USAGE_DESCRIPTION="To locate you"
npm install --save @ionic-native/geolocation@4

ionic cordova plugin add cordova-plugin-nativestorage
npm install --save @ionic-native/native-storage@4

ionic cordova plugin add cordova-plugin-advanced-http
npm install --save @ionic-native/http@4

ionic cordova plugin add cordova-plugin-chooser
npm install --save @ionic-native/chooser@4

npm uninstall  --save ionic-native-http-connection-backend 
